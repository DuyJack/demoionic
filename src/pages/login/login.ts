import { Component, OnDestroy } from "@angular/core";
import { ToastController, NavController } from "ionic-angular";
import { DataValid } from "../../helper/datavalid";
// import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { AccountServiceProvider } from "../../providers/account-service/account-service";
import { HomePage } from "../home/home";

@Component({
    selector: 'pageLogin',
    templateUrl: 'login.html',
})
export class LoginPage implements OnDestroy {
    username: string = ""
    password: string = ""
    disableLogin: boolean = false
    timeoutId: number = null
    timeLoginFailed: number = 0

    valid: DataValid = new DataValid()

    constructor(
        public navCtrl: NavController,
        public toastCtrl: ToastController,
        public accountService: AccountServiceProvider
    ) {

    }

    async signIn() {
        if (await this.accountService.loginAccount(this.username, this.password)) {
            this.navCtrl.setRoot(HomePage)
            this.showSigninSuccess()
        } else {
            this.timeLoginFailed++;
            if (this.timeLoginFailed >= 5) {
                this.blockButtonLogin()
            } else {
                this.showSigninFailed()
            }
        }
    }

    signUp() {
        if (!this.valid.checkUsernameValid(this.username)) {
            this.showErrorUsername()
            return;
        }
        if (!this.valid.checkPasswordValid(this.password)) {
            this.showErrorPassword()
            return;
        }
        if (this.accountService.registerAccount(this.username, this.password)) {
            this.showSignupSuccess()
        } else {
            this.showSignupFailed()
        }
    }

    showErrorUsername() {
        let toast = this.toastCtrl.create({
            message: "The length of username is between 8 and 20 characters.",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    showErrorPassword() {
        let toast = this.toastCtrl.create({
            message: "Password must contain at least 1 lơwercase, 1 upercase, 1 numeric and 1 speical character.",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    showSignupSuccess() {
        let toast = this.toastCtrl.create({
            message: "Sign up succeed",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    showSignupFailed() {
        let toast = this.toastCtrl.create({
            message: "Account is existed",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    showSigninSuccess() {
        let toast = this.toastCtrl.create({
            message: "Login succeed",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    showSigninFailed() {
        let toast = this.toastCtrl.create({
            message: "Login failed",
            duration: 2000,
            position: 'middle'
        });
        toast.present(toast);
    }

    blockButtonLogin() {
        let toast = this.toastCtrl.create({
            message: "Login failed 5 times, user can login in 1 minutes",
            duration: 4000,
            position: 'middle'
        });
        toast.present(toast);
        this.disableLogin = true;
        this.timeoutId = setTimeout(() => {
            this.disableLogin = false;
        }, 60000);
    }

    ngOnDestroy(): void {
        if (this.timeoutId) {
            clearTimeout(this.timeoutId)
        }
    }
}
