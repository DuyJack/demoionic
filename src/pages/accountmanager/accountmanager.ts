import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { Account } from '../../models/account';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the AccountmanagerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-accountmanager',
  templateUrl: 'accountmanager.html',
})
export class AccountmanagerPage implements OnDestroy {

  listAccount: Array<Account>
  subscription: Subscription

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public accountService: AccountServiceProvider
  ) {
    this.listAccount = new Array<Account>();
    console.log('this.accountService.listAccount', this.accountService.listAccount)
    console.log('listAccount', this.listAccount)
    this.listAccount = this.accountService.listAccount
    console.log('this.listAccount', this.listAccount)
    this.subscription = this.accountService.listAccountShow.subscribe((list: Array<Account>) => {
      this.listAccount = list
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountmanagerPage');
  }

  removeAccount(account: Account) {
    this.accountService.removeAccount(account);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
