import { NavController } from "ionic-angular";
import { Component, OnDestroy } from "@angular/core";
import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { LocationDetail } from "../../models/location-detail";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'page-test3',
    templateUrl: 'test3.html',
})
export class Test3Page implements OnDestroy {
    subscription: Subscription;
    location: LocationDetail;

    
    constructor(
        public navCtrl: NavController,
        public locationService: LocationServiceProvider
    ) {
        this.location = this.locationService.locationDetailCurrent;
        this.subscription = this.locationService.locationDetail.subscribe((location: LocationDetail) => {
            console.log("location ", location.address);
            this.location = location;
        });
    }


    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}