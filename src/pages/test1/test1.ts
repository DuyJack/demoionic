import { Component, OnInit, OnDestroy, OnChanges, DoCheck, AfterViewChecked, AfterViewInit, AfterContentChecked, AfterContentInit, SimpleChanges } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// import { HomePage } from '../home/home';
import { Article } from '../../models/article';

@Component({
	selector: 'page-test1',
	templateUrl: 'test1.html'
})
export class Test1Page implements OnInit, OnDestroy, OnChanges, DoCheck, AfterViewChecked, AfterViewInit, AfterContentChecked, AfterContentInit {
	
	article: Article = new Article();

	constructor(
		public navCtrl: NavController,
		private navParams: NavParams,
	) {
		if (this.navParams.get('data')) {
			this.article = this.navParams.get('data') as Article;
		}
	}

	ngAfterContentInit(): void {
		// console.log('Test1Page ngAfterContentInit');
	}

	ngOnInit(): void {
		console.log('Test1Page ngOnInit');
	}

	ngOnDestroy(): void {
		console.log('Test1Page ngOnDestroy');
	}

	ngOnChanges(changes: SimpleChanges): void {
		// console.log('Test1Page ngOnChanges', changes);
	}

	ngDoCheck(): void {
		// console.log('Test1Page ngDoCheck');
	}

	ngAfterViewChecked(): void {
		// console.log('Test1Page ngAfterViewChecked');
	}

	ngAfterViewInit(): void {
		// console.log('Test1Page ngAfterViewInit');
	}

	ngAfterContentChecked(): void {
		// console.log('Test1Page ngAfterContentChecked');
	}

	ionViewDidLoad() {
		console.log('Test1Page ionViewDidLoad');
	}

	ionViewWillEnter() {
		console.log('Test1Page ionViewWillEnter');
	}

	ionViewDidEnter() {
		console.log('Test1Page ionViewDidEnter');
	}

	ionViewWillLeave() {
		console.log('Test1Page ionViewWillLeave');
	}

	ionViewDidLeave() {
		console.log('Test1Page ionViewDidLeave');
	}

	ionViewWillUnload() {
		console.log('Test1Page ionViewWillUnload');
	}

	ionViewCanEnter() {
		console.log('Test1Page ionViewCanEnter');
		return true;
	}

	ionViewCanLeave() {
		console.log('Test1Page ionViewCanLeave');
		return true;
	}

	gotoHome() {
		this.navCtrl.pop();
	}

}