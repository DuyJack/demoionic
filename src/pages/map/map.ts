import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController, ToastController } from "ionic-angular";
import {
	// GoogleMaps,
	// GoogleMap,
	// GoogleMapsEvent,
	// Marker,
	// GoogleMapsAnimation,
	// MyLocation,
	LatLng
} from '@ionic-native/google-maps';
import { BackgroundGeolocation, BackgroundGeolocationResponse, BackgroundGeolocationCurrentPositionConfig } from '@ionic-native/background-geolocation';
import { LocationServiceProvider } from "../../providers/location-service/location-service";
import { LocationDetail } from "../../models/location-detail";

declare var google;

@Component({
	selector: 'page-map',
	templateUrl: 'map.html',
	// providers: [LocationServiceProvider]
})
export class MapPage {
	@ViewChild('map_canvas') mapElement: ElementRef;
	map;
	markerMe = null;
	i = 0;

	constructor(
		public navCtrl: NavController,
		public toastCtrl: ToastController,
		private backgroundGeolocation: BackgroundGeolocation,
		public locationService: LocationServiceProvider
	) {

	}

	ionViewDidEnter() {
		this.loadMap()
		if (this.map) {
			var _this = this;
			this.map.addListener('click', (e) => {
				// console.log('e', e);
				// console.log('LatLng', e.latLng.lat(), e.latLng.lng());
				_this.removeMarker();
				_this.createMarker(e.latLng, true);
				_this.moveTo(e.latLng);
			})
		}
	}

	loadMap() {
		// let latLng = new google.maps.LatLng(-34.9290, 138.6010);
		let mapOptions = {
			center: { lat: -34.9290, lng: 150.644 },
			zoom: 18,
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
		const config: BackgroundGeolocationCurrentPositionConfig = {
			timeout: 10000,
			maximumAge: 10,
			enableHighAccuracy: true
		};
		const _this = this;
		this.backgroundGeolocation.getCurrentLocation(config).then((location: BackgroundGeolocationResponse) => {
			const latLng = new google.maps.LatLng(location.latitude, location.longitude);
			_this.removeMarker();
			_this.createMarker(latLng);
			_this.moveTo(latLng)
		}).catch(err => console.log(err));
	}

	async onButtonClick() {
		const config: BackgroundGeolocationCurrentPositionConfig = {
			timeout: 10000,
			maximumAge: 10,
			enableHighAccuracy: true
		};
		this.removeMarker();
		const _this = this;
		this.backgroundGeolocation.getCurrentLocation(config).then((location: BackgroundGeolocationResponse) => {
			const latLng = new google.maps.LatLng(location.latitude, location.longitude);
			_this.createMarker(latLng);
			_this.moveTo(latLng);
		}).catch(err => console.log(err))
	}

	removeMarker() {
		if (this.markerMe) {
			this.markerMe.setMap(null);
			this.markerMe = null;
		}
	}

	createMarker(position: LatLng, haveUpdateLocationService = false) {
		if (this.markerMe === null) {
			this.markerMe = new google.maps.Marker({
				map: this.map,
				animation: google.maps.Animation.DROP,
				position: position
			});
		}
		const geocoder = new google.maps.Geocoder;
		geocoder.geocode({ 'location': position }, (res, status) => {
			// console.log('status_ ', status);
			// console.log('res_ ', res);
			if (status === 'OK' && res.length > 0) {
				if (res[0].formatted_address) {
					const address = res[0].formatted_address;
					const content = `<h4>${address}</h4>`;
					console.log('haveUpdateLocationService', haveUpdateLocationService);
					if (haveUpdateLocationService) {
						const location = new LocationDetail(position, address);
						this.locationService.updateLocationDetail(location)
					} else {
						this.locationService.updateLocationDetail()
					}
					this.addInfoWindow(this.markerMe, content);
				}
			}
		})
	}

	moveTo(position: LatLng) {
		if (this.map) {
			this.map.panTo(position);
		}
	}

	addInfoWindow(marker, content) {
		const infoWindow = new google.maps.InfoWindow({
			content: content
		});
		google.maps.event.addListener(marker, 'click', () => {
			infoWindow.open(this.map, marker);
		});
	}

	showToast(message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 2000,
			position: 'middle'
		});

		toast.present(toast);
	}
}

// @Component({
// 	selector: 'page-map',
// 	templateUrl: 'map.html'
// })
// export class MapPage {
// 	map: GoogleMap;

// 	constructor(
// 		public toastCtrl: ToastController,
// 		private backgroundGeolocation: BackgroundGeolocation
// 	) {
// 	}

// 	ionViewDidEnter() {
// 		this.loadMap();
// 	}

// 	loadMap() {
// 		// Create a map after the view is loaded.
// 		// (platform is already ready in app.component.ts)
// 		if (this.map === undefined || this.map === null) {
// 			this.map = GoogleMaps.create('map_canvas', {
// 				camera: {
// 					target: {
// 						lat: 43.0741704,
// 						lng: -89.3809802
// 					},
// 					zoom: 18,
// 					tilt: 30
// 				}
// 			});
// 		}
// 	}

	// async onButtonClick() {
	// 	console.log("a");

	// 	const config: BackgroundGeolocationCurrentPositionConfig = {
	// 		timeout: 10000,
	// 		maximumAge: 10,
	// 		enableHighAccuracy: true
	// 	};

	// 	this.backgroundGeolocation.getCurrentLocation(config).then((location: BackgroundGeolocationResponse) => {

	// 		console.log("location", location);
	// 		const latLng: LatLng = new LatLng(location.latitude, location.longitude);
	// 		this.map.moveCamera({
	// 			target: latLng,
	// 			zoom: 17
	// 		}).then(async () => {
	// 			console.log('move camera');
	// 			await this.map.clear();
	// 			const marker: Marker = this.map.addMarkerSync({
	// 				title: 'Here',
	// 				position: latLng,
	// 				animation: GoogleMapsAnimation.BOUNCE
	// 			});
	// 			marker.showInfoWindow();
	// 			marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
	// 				this.showToast('clicked!');
	// 			});
	// 		})
	// 		this.backgroundGeolocation.finish(); // FOR IOS ONLY
	// 	})
	// 		.catch(err => console.log(err));
	// 	this.backgroundGeolocation.start();
	// 	this.backgroundGeolocation.stop();
	// }

	// showToast(message: string) {
	// 	let toast = this.toastCtrl.create({
	// 		message: message,
	// 		duration: 2000,
	// 		position: 'middle'
	// 	});

	// 	toast.present(toast);
	// }
// }