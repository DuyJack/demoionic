import { Component, OnInit, OnDestroy, OnChanges, DoCheck, AfterViewChecked, AfterViewInit, AfterContentChecked, AfterContentInit, SimpleChanges } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';

import { Test1Page } from '../test1/test1';
import { ArticleServiceProvider } from '../../providers/article-service/article-service';
import { Article } from '../../models/article';
import { StorageServiceProvider } from '../../providers/storage-service/storage-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy, OnChanges, DoCheck, AfterViewChecked, AfterViewInit, AfterContentChecked, AfterContentInit {

  STATE_CATE = "categories"
  STATE_COUNTRY = "country"

  listArticles: Array<Article> = [];
  listArticlesShow: Array<Article> = [];
  listCountry: Array<String> = ["ae", "ar", "at", "au", "be", "bg", "br", "ca", "ch", "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr", "hk", "hu", "id", "ie", "il", "in", "it", "jp", "kr", "lt", "lv", "ma", "mx", "my", "ng", "nl", "no", "nz", "ph", "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg", "si", "sk", "th", "tr", "tw", "ua", "us", "ve", "za"];
  categories: Array<String> = ["business", "entertainment", "general", "health", "science", "sports", "technology"];
  country: String = "us";
  category: String = "business";
  textSearch: String = ""

  constructor(
    navParams: NavParams,
    public navCtrl: NavController,
    public articleService: ArticleServiceProvider,
    public toastCtrl: ToastController,
    public localstorage: StorageServiceProvider,
    private platform: Platform,
  ) {
    
  }

  ngAfterContentInit(): void {
    // console.log('HomePage ngAfterContentInit');
  }

  ngOnInit(): void {
    console.log('HomePage ngOnInit');
  }

  ngOnDestroy(): void {
    console.log('HomePage ngOnDestroy');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('HomePage ngOnChanges', changes);
  }

  ngDoCheck(): void {
    // console.log('HomePage ngDoCheck');
  }

  ngAfterViewChecked(): void {
    // console.log('HomePage ngAfterViewChecked');
  }

  ngAfterViewInit(): void {
    // console.log('HomePage ngAfterViewInit');
  }

  ngAfterContentChecked(): void {
    // console.log('HomePage ngAfterContentChecked');
  }

  ionViewDidLoad() {
    console.log('HomePage ionViewDidLoad');
    this.platform.ready().then(() => {
      this.loadState()
    })
  }

  ionViewWillEnter() {
    console.log('HomePage ionViewWillEnter');
  }

  ionViewDidEnter() {
  }

  ionViewWillLeave() {
    console.log('HomePage ionViewWillLeave');
  }

  ionViewDidLeave() {
    console.log('HomePage ionViewDidLeave');
  }

  ionViewWillUnload() {
    console.log('HomePage ionViewWillUnload');
    this.saveState();
  }

  ionViewCanEnter() {
    console.log('HomePage ionViewCanEnter');
    return true;
  }

  ionViewCanLeave() {
    console.log('HomePage ionViewCanLeave');
    return true;
  }

  async saveState() {
    await this.localstorage.set(this.STATE_CATE, this.category.toString());
    await this.localstorage.set(this.STATE_COUNTRY, this.country.toString());
  }

  async loadState() {
    const res = await this.localstorage.get(this.STATE_CATE)
    if (this.categories.indexOf(res) > -1) {
      this.category = res;
    }
    const res2 = await this.localstorage.get(this.STATE_COUNTRY)
    if (this.listCountry.indexOf(res2) > -1) {
      this.country = res2;
    }
    this.loadArticle();
  }

  loadArticle() {
    this.articleService.load(this.country, this.category).then(res => {
      const responses = res as Array<Article>;
      if (responses.length > 0) {
        this.listArticlesShow = responses;
      } else {
        this.listArticlesShow = responses;
      }
      this.saveState();
    }).catch(err => {
      console.log(err);
    })
  }

  compareFn(e1: String, e2: String): boolean {
    return e1 === e2;
  }

  onCountryChange(countrySelected: String) {
    this.loadArticle();
  }

  onChangeTextSearch(e) {
    this.textSearch = e.value;
    if (this.textSearch.length > 0) {
      if (this.listArticlesShow.length > 0) {
        if (this.listArticles.length === 0) {
          this.listArticles = [...this.listArticlesShow];
        }
        this.listArticlesShow = this.listArticles.filter(article => article.title.toLowerCase().search(e.value.toLowerCase()) > -1);
      }
    } else {
      this.listArticlesShow = [...this.listArticles];
      this.listArticles = [];
    }
  }

  selectCategory(cate: String) {
    if (cate !== this.category) {
      this.category = cate;
      this.loadArticle();
    }
  }

  backToTest1(article: Article) {
    this.navCtrl.push(Test1Page, { data: article });
  }

}
