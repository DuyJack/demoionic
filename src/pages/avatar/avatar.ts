import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { AccountServiceProvider } from '../../providers/account-service/account-service';

/**
 * Generated class for the AvatarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-avatar',
  templateUrl: 'avatar.html',
})
export class AvatarPage {
  avatarLink: string = ""
  showConfirm: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private accountService: AccountServiceProvider,
    private imagePicker: ImagePicker,
    private camera: Camera
  ) {
    if (this.accountService.accountCurrent.isLogin) {
      this.avatarLink = this.accountService.accountCurrent.avatar
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AvatarPage');
  }

  openLibrary() {
    console.log('openLibrary')
    this.imagePicker.hasReadPermission().then((isAllow: boolean) => {
      if (isAllow) {
        const options: ImagePickerOptions = {
          maximumImagesCount: 1,
          width: 100,
          height: 100,
          outputType: 1,
        }
        this.imagePicker.getPictures(options).then((results) => {
          for (var i = 0; i < results.length; i++) {
            // console.log('Image URI: ' + results[i])
            this.avatarLink = `data:image/jpeg;base64,${results[i]}`
            console.log(this.avatarLink);
            this.showConfirm = true;
          }
        }, (err) => { });
      } else {
        this.imagePicker.requestReadPermission().then(res => {
          console.log('res', res);
        }).catch(err => console.log(err));
      }
    })
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      this.avatarLink = `data:image/jpeg;base64,${imageData}`
      this.showConfirm = true;
    }, (err) => {
      console.log(err)
    });
  }

  async save() {
    this.accountService.accountCurrent.avatar = this.avatarLink;
    await this.accountService.updateAvatar(this.avatarLink);
    this.navCtrl.pop();
  }

  cancel() {
    this.navCtrl.pop();
  }

  goBack() {
    this.navCtrl.pop();
  }

}
