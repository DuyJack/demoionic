import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotoLibrary } from '@ionic-native/photo-library';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

import { PhotoLibraryPage } from './photo-library';
import { ImagePipe } from '../../pipes/image/image';
// import { EscapeHtmlPipe } from '../../helper/escape-html-pipe.pipe';

@NgModule({
  declarations: [
    PhotoLibraryPage,
    // EscapeHtmlPipe
    ImagePipe
  ],
  imports: [
    IonicPageModule.forChild(PhotoLibraryPage),
  ],
  exports: [
    // EscapeHtmlPipe
    ImagePipe
  ],
  providers: [
    PhotoLibrary,
    File,
    FilePath,
  ]
})
export class PhotoLibraryPageModule { }
