import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhotoLibrary, GetLibraryOptions } from '@ionic-native/photo-library';
import { File, Entry } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Base64 } from '@ionic-native/base64';
import { VideoPlayer } from '@ionic-native/video-player';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { VideoEditor, CreateThumbnailOptions } from '@ionic-native/video-editor';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';

import { Media } from '../../models/media'
import { MapPage } from '../map/map';
/**
 * Generated class for the PhotoLibraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photo-library',
  templateUrl: 'photo-library.html',
})
export class PhotoLibraryPage {
  listMedia: Array<Media> = new Array<Media>()

  TypePhoto: string[] = ['.jpeg', '.jpg', '.png', '.gif', '.tiff', '.heic']
  TypeVideo: string[] = ['.mp4']
  ListSkip: string[] = ['Android', 'Notifications', 'Alarms']
  win: any = window;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public photoLibrary: PhotoLibrary,
    public cd: ChangeDetectorRef,
    public file: File,
    public filePath: FilePath,
    private base64: Base64,
    private videoPlayer: VideoPlayer,
    private camera: Camera,
    private videoEditor: VideoEditor,
    private mediaCapture: MediaCapture
  ) {

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad')
    this.photoLibrary.requestAuthorization().then(res => {
      // console.log('res', res)
      this.loadLibrary()
      // this.loadVideo()
    }).catch(err => console.log(err))
  }

  loadLibrary() {
    this.photoLibrary.requestAuthorization().then(() => {
      const option: GetLibraryOptions = {
        includeVideos: true,
        includeAlbumData: true,
        quality: 0.5
      }
      let that = this;
      this.photoLibrary.getLibrary(option).subscribe({
        next: library => {
          // console.log('library', library);
          library.forEach(function (libraryItem) {
            console.log('libraryItem', libraryItem);
            // console.log(libraryItem.id);          // ID of the photo
            // console.log(libraryItem.photoURL);    // Cross-platform access to photo
            // console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
            // console.log(libraryItem.fileName);
            // console.log(libraryItem.width);
            // console.log(libraryItem.height);
            // console.log(libraryItem.creationDate);
            // console.log(libraryItem.latitude);
            // console.log(libraryItem.longitude);
            // console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
            const media = new Media()
            // console.log('path', temp[1])
            // media.url = win.Ionic.WebView.convertFileSrc(`file://` + temp[1])
            media.isCheck = false
            media.isVideo = that.isFileVideo(libraryItem.fileName)
            media.isPhoto = that.isFilePhoto(libraryItem.fileName)
            media.url = libraryItem.thumbnailURL
            media.fullPath = libraryItem.photoURL
            console.log('media', media)
            // console.log(that.listMedia)
            that.listMedia.push(media)
            // console.log(that.listMedia)
          });
          this.cd.detectChanges();
        },
        error: err => { console.log('could not get photos'); },
        complete: () => { console.log('done getting photos'); }
      });
    })
      .catch(err => console.log('permissions weren\'t granted'));
  }

  loadVideo() {
    // this.getInfoEntry(this.file.dataDirectory);
    // this.getInfoEntry(this.file.documentsDirectory);
    this.getInfoEntry(this.file.externalRootDirectory).then().catch()
    // this.getInfoEntry("file:///storage/emulated/0/DCIM/")
  }

  listPath: Array<string> = new Array<string>()

  getInfoEntry(source: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      let isSource: boolean = false
      do {
        let result: Entry[]
        let subFolder: string
        if (!isSource) {
          console.log("source", source)
          result = await this.file.listDir(source, '')
          isSource = true
        } else {
          subFolder = this.listPath[this.listPath.length - 1]
          console.log("source", subFolder)
          result = await this.file.listDir(source, subFolder)
          if (this.listPath.length > 0) {
            this.listPath.pop()
          }
        }
        // console.log('result', result)
        for (let i = 0; i < result.length; i++) {
          const entry = result[i];
          if (entry.isDirectory && entry.name != '.' && entry.name != '..') {
            if (!this.isSkip(entry.name)) {
              if (subFolder) {
                this.listPath.push(`${subFolder}/${entry.name}`)
              } else {
                this.listPath.push(entry.name)
              }
            }
          } else if (entry.isFile) {
            if (this.isFilePhoto(entry.name) || this.isFileVideo(entry.name)) {
              const media = new Media()
              if (this.isFilePhoto(entry.name)) {
                try {
                  media.url = await this.convertToBase64(entry.nativeURL)
                } catch (err) {
                  console.log('err', err)
                }
              } else if (this.isFileVideo(entry.name)) {
                console.log('media', entry)
                let data = null;
                try {
                  data = await this.createThumbnail(entry.nativeURL, entry.name)
                }
                catch (err) {
                  console.log('err', err)
                }
                if (data) {
                  console.log('data', data)
                  // const base64 = await this.generateFromImage(data, 200, 200, 0.5)
                  // const filePoster = await this.saveFileFromBase64(entry.name, base64)
                  // console.log('filePoster', filePoster)
                  media.url = this.win.Ionic.WebView.convertFileSrc(`file://${data}`)
                  // console.log('media.url', media.url)
                }
              }
              media.isPhoto = this.isFilePhoto(entry.name)
              media.isVideo = this.isFileVideo(entry.name)
              media.fullPath = this.win.Ionic.WebView.convertFileSrc(entry.nativeURL)
              this.listMedia.push(media)
            }
          }
        }
        console.log(this.listPath.length);
      } while (this.listPath.length > 0)
      console.log('listMedia', this.listMedia);
      resolve('done')
    })
  }

  isSkip(foldername: string): boolean {
    for (let type of this.ListSkip) {
      if (foldername === type) {
        return true
      }
    }
    return false
  }

  isFileVideo(filename: string): boolean {
    if (filename) {
      for (let type of this.TypeVideo) {
        if (filename.toLowerCase().endsWith(type)) {
          return true
        }
      }
    }
    return false
  }

  isFilePhoto(filename: string): boolean {
    // console.log('filename', filename);
    if (filename) {
      for (let type of this.TypePhoto) {
        // console.log('filename.toLowerCase()', filename.toLowerCase());
        // console.log('type', type);
        // console.log('filename.toLowerCase().endsWith(type)', filename.toLowerCase().endsWith(type));
        if (filename.toLowerCase().endsWith(type)) {
          return true
        }
      }
    }
    return false
  }

  selectItem(item: Media) {
    console.log('selectItem', item)
    const index = this.listMedia.indexOf(item)
    console.log('index', index)
    if (index > -1) {
      this.listMedia[index].isCheck = !this.listMedia[index].isCheck
    }
    console.log('this.listMedia', this.listMedia);
  }

  convertToBase64(filePath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.base64.encodeFile(filePath).then(async (base64File: string) => {
        try {
          const data = await this.generateFromImage(base64File, 200, 200, 0.5)
          resolve(data);
        } catch (err) {
          console.log('err', err)
        }
      }, (err) => {
        console.log(err);
        reject(err)
      });
    })
  }

  generateFromImage(img, MAX_WIDTH: number = 700, MAX_HEIGHT: number = 700, quality: number = 1): Promise<string> {
    return new Promise((resolve, reject) => {
      var canvas: any = document.createElement("canvas");
      var image = new Image();

      image.onload = () => {
        var width = image.width;
        var height = image.height;

        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");

        ctx.drawImage(image, 0, 0, width, height);

        // IMPORTANT: 'jpeg' NOT 'jpg'
        var dataUrl = canvas.toDataURL('image/jpeg', quality);

        resolve(dataUrl)
      }
      image.src = img;
    })
  }

  openOtherPage() {
    this.navCtrl.push(MapPage)
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData: string) => {
      this.convertToBase64(imageData).then((res: string) => {
        const media = new Media()
        media.fullPath = imageData
        media.isCheck = true
        media.isPhoto = true
        media.isVideo = false
        media.url = res
        this.listMedia.push(media)
      })
    })
  }

  captureVideo() {
    const options: CaptureImageOptions = { limit: 1 };
    this.mediaCapture.captureVideo(options).then(
      (data: MediaFile[]) => {
        data.forEach(async (video: MediaFile, index: number) => {
          const data = await this.createThumbnail(video.fullPath, video.name)
          const media = new Media();
          media.isCheck = true
          media.isPhoto = false
          media.isVideo = true
          media.url = this.win.Ionic.WebView.convertFileSrc(`file://${data}`)
          media.fullPath = this.win.Ionic.WebView.convertFileSrc(video.fullPath)
          console.log('media', media)
          this.listMedia.push(media)
        })
      },
      (err: CaptureError) => console.error(err)
    ).catch(err => {
      console.log('err', err)
    })
  }

  saveFileFromBase64(filename: string, base64: string): Promise<string> {
    return new Promise((resolve, reject) => {
      let realData = base64.split(",")[1];
      let blob = this.b64toBlob(realData, 'image/jpeg');
      let UUID = filename;
      this.file.checkDir(this.file.externalApplicationStorageDirectory, 'DirectorioFotos')
        .then(_ => {
          this.file.writeFile(this.file.externalApplicationStorageDirectory + 'DirectorioFotos/', UUID, blob).then(response => {
            console.log('response', response);
            resolve(response)
          }).catch(err => {
            console.log('err', err);
          })
        })
        .catch(err => {
          this.file.createDir(this.file.externalApplicationStorageDirectory, 'DirectorioFotos', false).then(result => {
            this.file.writeFile(this.file.externalApplicationStorageDirectory + 'DirectorioFotos/', UUID, blob).then(response => {
              console.log('response', response);
              resolve(response)
            }).catch(err => {
              console.log('err', err);
            })
          })
        });
    })
  }

  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  createThumbnail(fileUri: string, nameFile: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const name = nameFile.split('.');
      let filename = "";
      if (name.length > 1) {
        for (let i = 0; i < name.length - 1; i++) {
          filename = filename + name[i]
        }
      }
      console.log('filename', filename)
      let thumbnailoption: CreateThumbnailOptions = {
        fileUri: fileUri,
        quality: 80,
        atTime: 2,
        outputFileName: filename,
      }
      this.videoEditor.createThumbnail(thumbnailoption).then((thumbnailPath) => {
        console.log("Thumbnail Responce =>", thumbnailPath)
        resolve(thumbnailPath)
      }).catch((err) => {
        console.log("Thumbnail Responce Error=>", err)
      })
    })
  }
}
