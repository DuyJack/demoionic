import { Component, ViewChild, OnDestroy } from '@angular/core';
import { Platform, Nav, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Subscription } from 'rxjs/Subscription';

import { HomePage } from '../pages/home/home';
// import { Test1Page } from '../pages/test1/test1';
import { MapPage } from '../pages/map/map';
import { LoginPage } from '../pages/login/login';
import { Test3Page } from '../pages/test3/test3';
import { LocationServiceProvider } from '../providers/location-service/location-service';
import { LocationDetail } from '../models/location-detail';
import { AccountServiceProvider } from '../providers/account-service/account-service';
import { Account } from '../models/account';
import { AccountmanagerPage } from '../pages/accountmanager/accountmanager';
import { AvatarPage } from '../pages/avatar/avatar';
import { PhotoLibraryPage } from '../pages/photo-library/photo-library';

class Tab {
  title: String
  isShow: boolean = false
  component: any

  constructor(title: String = "", component: any, isShow: boolean = true) {
    this.title = title
    this.component = component
    this.isShow = isShow
  }
}

@Component({
  templateUrl: 'app.html',
  // providers: [LocationServiceProvider]
})
export class MyApp implements OnDestroy {
  private defaultAvatar: string = "assets/imgs/avatar_default.png";

  rootPage: any = HomePage;
  avatarLink: string = this.defaultAvatar;

  private avatarPage = new Tab('', AvatarPage, false)
  private tabLocation = new Tab('Location info', Test3Page, false)
  private tabLogout = new Tab('Logout', null, false)
  private tabLogin = new Tab("Login", LoginPage, true)
  private tabHome = new Tab("Home", HomePage)
  private tabMap = new Tab("Map", MapPage)
  private tabAccountManager = new Tab("Account Manage", AccountmanagerPage, false)
  private tabPhoto = new Tab("Media Storage", PhotoLibraryPage, false)
  pages: Array<Tab> = [
    this.tabLogin,
    this.tabHome,
    this.tabMap,
    this.tabLocation,
    this.tabAccountManager,
    this.tabPhoto,
    this.tabLogout,
    // { title: 'Home', component: HomePage },
    // { title: 'Map', component: MapPage },
  ];
  subscription: Subscription
  accountSubscription: Subscription

  @ViewChild(Nav) navCtrl: Nav

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menu: MenuController,
    public alertCtrl: AlertController,
    private locationService: LocationServiceProvider,
    private accountService: AccountServiceProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.subscription = this.locationService.locationDetail.subscribe((location: LocationDetail) => {
        console.log("location", location);
        if (location) {
          this.tabLocation.isShow = true;
        } else {
          this.tabLocation.isShow = false;
        }
      })
      this.accountSubscription = this.accountService.account.subscribe((account: Account) => {
        if (account) {
          if (account.isLogin) {
            this.avatarLink = account.avatar
            this.tabPhoto.isShow = true
            this.tabLogin.isShow = false
            this.tabLogout.isShow = true
            if (account.isAdmin()) {
              this.tabAccountManager.isShow = true
            }
          }
        } else {
          this.avatarLink = this.defaultAvatar
          this.tabPhoto.isShow = false
          this.tabLogin.isShow = true;
          this.tabLogout.isShow = false;
          this.tabAccountManager.isShow = false;
        }
      })
      this.accountService.loadAccountSignined();
      this.accountService.getAccountRegister();
    });
  }

  openPage(page: Tab) {
    console.log("page", page);
    if (page) {
      if (page === this.tabLogout) {
        this.logout()
      } else if (page === this.tabPhoto && this.rootPage !== page.component) {
        console.log('push')
        this.navCtrl.push(this.tabPhoto.component)
      } else if (this.rootPage !== page.component) {
        console.log('Reset')
        this.navCtrl.setRoot(page.component);
        this.rootPage = page.component;
      }
      this.menu.close();
    }
  }
  logout() {
    const confirm = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want logout this account?',
      buttons: [
        {
          text: 'No',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.accountService.logout().then(() => {
              if (this.rootPage !== this.tabHome.component) {
                this.navCtrl.setRoot(this.tabHome.component);
                this.rootPage = this.tabHome.component;
              }
            }).catch(err => console.log(err))
          }
        }
      ]
    });
    confirm.present();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.accountSubscription.unsubscribe();
  }

  openAvatar() {
    if (this.accountService.accountCurrent) {
      if (this.accountService.accountCurrent.isLogin) {
        this.navCtrl.push(this.avatarPage.component);
        this.menu.close();
        return
      }
    }
    this.openPage(this.tabLogin)
  }
}
