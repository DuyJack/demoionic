import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { NativeStorage } from '@ionic-native/native-storage';
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { VideoPlayer } from '@ionic-native/video-player';
import { VideoEditor } from '@ionic-native/video-editor';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Test1Page } from '../pages/test1/test1'
import { ArticleServiceProvider } from '../providers/article-service/article-service';
import { HttpClientModule } from '@angular/common/http';
import { MapPage } from '../pages/map/map';
import { Test3Page } from '../pages/test3/test3';
import { LocationServiceProvider } from '../providers/location-service/location-service';
import { StorageServiceProvider } from '../providers/storage-service/storage-service';
import { LoginPage } from '../pages/login/login';
import { AccountServiceProvider } from '../providers/account-service/account-service';
import { AccountmanagerPage } from '../pages/accountmanager/accountmanager';
import { AvatarPage } from '../pages/avatar/avatar';
import { PhotoLibraryPageModule } from '../pages/photo-library/photo-library.module';
import { PhotoLibraryPage } from '../pages/photo-library/photo-library';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Test1Page,
    Test3Page,
    LoginPage,
    MapPage,
    AvatarPage,
    AccountmanagerPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    // PipesModule,
    LazyLoadImageModule,
    PhotoLibraryPageModule
  ],
  bootstrap: [IonicApp],
  exports: [
    
  ],
  entryComponents: [
    MyApp,
    HomePage,
    Test1Page,
    Test3Page,
    LoginPage,
    MapPage,
    AvatarPage,
    AccountmanagerPage,
    PhotoLibraryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    ImagePicker,
    Camera,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ArticleServiceProvider,
    BackgroundGeolocation,
    LocationServiceProvider,
    StorageServiceProvider,
    AccountServiceProvider,
    Base64,
    VideoPlayer,
    VideoEditor,
    MediaCapture,
  ]
})
export class AppModule {}
