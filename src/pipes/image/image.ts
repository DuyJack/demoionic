import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ImagePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'imagePipe', pure: false
})
export class ImagePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  /**
   * Takes a value and makes it lowercase.
   */
  transform(url: string) {
    console.log('url', url.startsWith)
    if (url.startsWith) {
      const res = url.startsWith('cdvphotolibrary://') ? this.sanitizer.bypassSecurityTrustUrl(url) : url;
      console.log('res', res);
      return res
    }
    return url
  }
}
