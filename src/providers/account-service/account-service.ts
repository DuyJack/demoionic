import { Injectable } from '@angular/core';
import { Account } from '../../models/account';
import { StorageServiceProvider } from '../storage-service/storage-service';
import { Subject } from 'rxjs/Subject';

/*
  Generated class for the AccountServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccountServiceProvider {

  accountCurrent: Account = new Account();
  listAccount: Array<Account> = new Array()
  private adminAccount: Account;

  private accountSource = new Subject<Account>();
  account = this.accountSource.asObservable()

  private listAccountSource = new Subject<Array<Account>>();
  listAccountShow = this.listAccountSource.asObservable();

  constructor(public localStorage: StorageServiceProvider) {
    this.adminAccount = new Account();
    this.adminAccount.username = "admin";
    this.adminAccount.password = "admin";
  }

  registerAccount(username: string, password: string): boolean {
    if (this.adminAccount.isUserExisted(username)) {
      return false;
    }
    for (let i = 0; i < this.listAccount.length; i++) {
      const accountTemp = new Account()
      accountTemp.setData(this.listAccount[i])
      if (accountTemp.isUserExisted(username)) {
        return false
      }
    }
    const account = new Account();
    account.username = username;
    account.password = password;
    console.log('this.listAccount', this.listAccount);
    this.listAccount.push(account);
    console.log('this.listAccount', this.listAccount);
    this.listAccountSource.next(this.listAccount);
    this.localStorage.set("account", JSON.stringify(this.listAccount));
    return true;
  }

  async loginAccount(username: string, password: string): Promise<boolean> {
    if (this.adminAccount.canLogin(username, password)) { // check account admin
      console.log('this.accountCurrent', this.accountCurrent);
      console.log('this.adminAccount', this.adminAccount);
      if (!this.accountCurrent) {
        this.accountCurrent = new Account();
      }
      Object.assign(this.accountCurrent, this.adminAccount);
      this.accountCurrent.isLogin = true;
      this.accountSource.next(this.accountCurrent);
      await this.saveAccountLogined()
      return true;
    }
    for (let i = 0; i < this.listAccount.length; i++) { // check account normal
      const account: Account = new Account();
      account.setData(this.listAccount[i]);
      if (account.canLogin(username, password)) {
        console.log('this.listAccount[i]', this.listAccount[i])
        console.log('account', account)
        this.accountCurrent = account
        this.accountCurrent.isLogin = true
        this.accountSource.next(this.accountCurrent)
        await this.saveAccountLogined()
        return true
      }
    }
    return false;
  }

  public async getAccountRegister() {
    try {
      const account = await this.localStorage.get("account")
      console.log('JSON.parse(account)', JSON.parse(account));
      console.log('this.listAccount', this.listAccount);
      this.listAccount = this.listAccount.concat(JSON.parse(account));
      console.log('this.listAccount', this.listAccount);
    } catch (err) {
      console.log(err);
    }
  }

  public async loadAccountSignined() {
    let user = null;
    try {
      user = await this.localStorage.get("user");
    } catch (err) {
      console.log(err);
    }
    if (user) {
      console.log('user', user);
      const acc = new Account();
      acc.setData(JSON.parse(user));
      this.accountCurrent = acc;
      this.accountSource.next(this.accountCurrent);
    }
  }

  private async saveAccountLogined() {
    if (this.accountCurrent) {
      try {
        await this.localStorage.set("user", JSON.stringify(this.accountCurrent));
      } catch (err) {
        console.log(err);
      }
    }
  }

  async logout() {
    if (this.accountCurrent) {
      try {
        this.localStorage.set("user", null);
      } catch (err) {
        console.log(err);
      }
      this.accountCurrent = null;
      this.accountSource.next(null);
    }
  }

  removeAccount(account: Account) {
    this.listAccount = this.listAccount.filter((e, i) => e.username !== account.username);
    this.localStorage.set("account", JSON.stringify(this.listAccount));
    this.listAccountSource.next(this.listAccount);
  }

  async updateAvatar(link: string) {
    if (this.accountCurrent) {
      if (!this.accountCurrent.isAdmin()) {
        this.accountCurrent.avatar = link
        await this.saveAccountLogined()
        const index = this.listAccount.findIndex(e => this.accountCurrent.isUserExisted(e.username.toString()))
        if (index > -1) {
          this.listAccount[index].avatar = link
          this.localStorage.set("account", JSON.stringify(this.listAccount));
        }
        this.accountSource.next(this.accountCurrent);
      }
    }
  }
}
