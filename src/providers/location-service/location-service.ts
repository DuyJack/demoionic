import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { LocationDetail } from '../../models/location-detail';

/*
  Generated class for the LocationServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationServiceProvider {
  public locationDetailCurrent: LocationDetail = new LocationDetail()
  private locationDetailSource = new Subject<LocationDetail>();

  locationDetail = this.locationDetailSource.asObservable();

  constructor() {
    console.log('Hello LocationServiceProvider Provider');
  }

  updateLocationDetail(location: LocationDetail = null) {
    // console.log('location', location);
    // console.log('locationDetail', this.locationDetail);
    this.locationDetailCurrent = location;
    this.locationDetailSource.next(location);
    // console.log(this.locationDetailSource);
  }

}
