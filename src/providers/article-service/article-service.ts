import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Article } from '../../models/article';

/*
  Generated class for the ArticleServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ArticleServiceProvider {

  private DOMAIN: String = "https://newsapi.org/v2/top-headlines";
  private API_KEY: String = "06849ffbcb1542fb9e8708e597c28a67";

  constructor(private http: HttpClient) {
    console.log('Hello ArticleServiceProvider Provider');
  }

  load(country: String = "us", category: String = "business") {
    return new Promise((resolve, reject) => {
      const API_URL = `${this.DOMAIN}?country=${country}&category=${category}&apiKey=${this.API_KEY}`;
      this.http.get(API_URL).subscribe(res => {
        const response: ResponseHttp<Article> = res as ResponseHttp<Article>;
        if (response.totalResults > 0) {
          resolve(response.articles);
        } else {
          resolve([]);
        }
      }, err => {
        reject(err);
      })
    })
  }

}
