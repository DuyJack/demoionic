import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';

/*
  Generated class for the StorageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServiceProvider {

  constructor(private localsecureStorage: NativeStorage) {

  }

  async get(key: string): Promise<string> {
    if (this.localsecureStorage) {
      const res = await this.localsecureStorage.getItem(key);
      if (res) {
        return res;
      }
    }
    return null;
  }

  set(key: string, value: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.localsecureStorage) {
        this.localsecureStorage.setItem(key, value).then(
          data => {
            console.log(data)
            resolve(true)
          },
          err => {
            console.log(err)
            reject(false)
          }
        ).catch(err => console.log(err))
      } else {
        reject(false);
      }
    })
  }

  remove(key: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.localsecureStorage) {
        this.localsecureStorage.remove(key).then(
          data => {
            console.log(data)
            resolve(true)
          },
          err => {
            console.log(err)
            reject(false)
          }
        ).catch(err => console.log(err))
      } else {
        reject(false)
      }
    })
  }

}
