import { SafeStyle, SafeUrl } from "@angular/platform-browser"

export class Media {
    isCheck: boolean = false
    url: string | SafeUrl // thumnailb
    isVideo: boolean
    isPhoto: boolean
    fullPath: string
}