export class Article {
    source: Source = new Source()
    author: String = ""
    title: String = ""
    description: String = ""
    url: String = ""
    urlToImage: String = ""
    publishedAt: String = ""
    content: String = ""
}

class Source {
    id: String
    name: String
}