import { LatLng } from "@ionic-native/google-maps";

export class LocationDetail {
    location: LatLng;
    address: String;

    constructor(loc: LatLng = new LatLng(0, 0), add: String = "") {
        this.location = loc;
        this.address = add;
    }
}