class ResponseHttp<T> {
    status: String
    totalResults: number
    articles: Array<T>
}