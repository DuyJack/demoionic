export class Account {
    isLogin: boolean = false;
    username: String = "";
    password: String = "";

    avatar: string = "assets/imgs/avatar_default.png";

    setData(data: object) {
        Object.assign(this, data);
    }

    canLogin(username: string, password: string): boolean {
        return username === this.username && password === this.password
    }

    isUserExisted(username: string) {
        return username === this.username;
    }

    isAdmin(): boolean {
        return this.username === "admin" && this.username === this.password
    }
}