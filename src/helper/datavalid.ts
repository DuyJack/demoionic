export class DataValid {
    usernameTest = new RegExp("^(?=.{8,20})")
    passwordTest = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])")

    checkUsernameValid(username: string): boolean {
        return this.usernameTest.test(username);
    }

    checkPasswordValid(password: string): boolean {
        return this.passwordTest.test(password);
    }

}