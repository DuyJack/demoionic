import { Component, Input, ViewChild, ElementRef } from '@angular/core';

/**
 * Generated class for the VideoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'videoComponent',
  templateUrl: 'video.html'
})
export class VideoComponent {

  @Input() thumbnail: string;
  @Input() isCheck: boolean = false
  @Input() path: string

  @ViewChild('video') videoElement: ElementRef;

  showVideo: boolean = false

  selectVideo() {
    this.showVideo = !this.showVideo
    console.log('this.videoElement', this.videoElement)
  }
}
